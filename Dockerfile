FROM node:12.22.0 as builder

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install

COPY . .

RUN npm run build:ssr

RUN npm run prerender

FROM nginx:latest

COPY --from=builder /usr/src/app/dist/WISDOMs-UI /usr/share/nginx/html

COPY default.conf /etc/nginx/conf.d/default.conf
